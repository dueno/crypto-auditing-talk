#!/bin/bash

if ! test -d demo-scripts; then
    echo "call this script from the top-level directory"
    exit 1
fi

. demo-scripts/demo-magic/demo-magic.sh

DEMO_PROMPT="${CYAN}fedora\$ ${WHITE}"

SCRIPTS=$HOME/devel/crypto-auditing/scripts
GRAFANA=$HOME/devel/grafana-test

clear

TYPE_SPEED=20

pe "crypto-auditing-client --scope tls --format json"

clear

pe "rm -f audit.cborseq"

pe "crypto-auditing-client --scope tls --format cbor --output audit.cborseq"

clear

pe "crypto-auditing-log-parser audit.cborseq"

pe "readelf -nW /usr/lib64/libgnutls.so.30 | grep -A5 build-id"

pe "crypto-auditing-log-parser audit.cborseq | python \$SCRIPTS/flamegraph.py --format html -"

pe "gio open flamegraph.html"

clear

pe "rm -f audit.cborseq"

pe "crypto-auditing-client --scope tls --format cbor --output audit.cborseq"

clear

pe "crypto-auditing-log-parser audit.cborseq | python \$SCRIPTS/flamegraph.py --format html -"

pe "gio open flamegraph.html"

p ""
