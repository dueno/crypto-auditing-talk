# crypto-auditing-talk

Slides for "Are our systems using up-to-date cryptography?" at DevConf.CZ 2023.

## Building

0. Install pandoc and the dependencies:
```console
$ dnf install -y make mozilla-fira-mono-fonts mozilla-fira-sans-fonts pandoc \
                 python3-pip texlive-beamer texlive-beamertheme-metropolis \
                 texlive-bussproofs texlive-fira texlive-framed \
                 texlive-gsftopk texlive-metafont texlive-mfware \
                 texlive-minted texlive-multirow texlive-pgf-umlsd \
                 texlive-ulem texlive-unicode-math texlive-xetex which
$ pip3 install pandoc-minted
```
1. `make`

## License

MIT
