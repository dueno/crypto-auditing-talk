---
title: Are our systems using up-to-date cryptography?
subtitle: Open Source Summit Japan 2023
author: Daiki Ueno
organization: Red Hat
date: December 5, 2023
sansfont: Fira Sans
monofont: Fira Mono
slide-numbers: true
theme: metropolis
header-includes:
  - \usecolortheme{crane}
  - \definecolor{ossummit}{HTML}{DC2614} 
  - \setbeamercolor{frametitle}{fg=white,bg=ossummit}
  - \setbeamercolor{block title}{bg=white!60!ossummit}
  - \setbeamercolor{block body}{bg=white!90!ossummit}
  - \setbeamercolor{title separator}{fg=ossummit}
  - \usepackage{minted}
  - \usemintedstyle{tango}
  - \usepackage{tikz}
  - \usetikzlibrary{fit}
  - \usetikzlibrary{positioning}
  - \usetikzlibrary{shapes}
  - \usepackage{pgf-umlsd}
  - \usepgflibrary{arrows}
  - \usepackage[medium]{FiraSans}
---

## Who am I

::: {.columns}

:::: {.column}
![](ueno.jpg){width=70%}
::::

:::: {.column width="60%"}
Daiki Ueno

* Software Engineer at Red Hat
* Working on low-level crypto packages in Red Hat Enterprise Linux (RHEL)
* Spent 6 years in Czech Republic, returned to Japan in 2022

::::

:::

## What is this talk about

\Large crypto-auditing

\normalsize
Monitor cryptographic operations performed on a system

## Why?

**Cryptography is used everywhere**

- Protecting network traffic (TLS, IPsec, SSH)
- Encrypting disks
- Signing container images

**Every cryptographic algorithm has its own lifetime**

- 3DES, MD5, SHA-1, RSA with small key sizes, $\cdots$
- Classical public-key algorithms in post-quantum era

\large

**$\rightarrow$ Keeping legacy algorithms enabled could eventually make the system vulnerable**

\flushright
![](oke_riron2.png){width=1.5cm}
![](oke_riron1.png){width=1.5cm}

## Case study: SHA-1 sunset

SHA-1 is considered broken for the purpose of digital signatures

- *"[The first collision for full SHA-1](https://shattered.io/static/shattered.pdf)"*, Stevens, Bursztein, Karpman, Albertini, and Markov, 2017
- *"[SHA-1 is a Shambles](https://sha-mbles.github.io/)"*, Laurent and Peyrin, 2020
- A chosen-prefix collision attack cost only 45k USD in 2020

## Case study: Timing side-channels in RSA PKCS#1 v1.5 encryption

Using RSA encryption with PKCS#1 v1.5 padding is prone to be vulnerable against side-channels

- *"[Chosen Ciphertext Attacks Against Protocols Based on the RSA Encryption Standard PKCS #1](https://archiv.infsec.ethz.ch/education/fs08/secsem/bleichenbacher98.pdf)"*, Bleichenbacher, 1998
- *"[Return Of Bleichenbacher’s Oracle Threat (ROBOT)](https://www.usenix.org/conference/usenixsecurity18/presentation/bock)"*, Böck, Somorovsky, and Young, 2018
- *"[Everlasting ROBOT: the Marvin Attack](https://people.redhat.com/~hkario/marvin/marvin-attack-paper.pdf)"*, Kario, 2023

## Case study: Triple handshake attack on TLS 1.2

TLS 1.2 key derivation is vulnerable to a man-in-the-middle attack, where attacker is able to inpersonate a client

- *"[Triple Handshakes and Cookie Cutters: Breaking and Fixing Authentication over TLS](https://www.mitls.org/downloads/tlsauth.pdf)"*, Bhargavan, Delignat-Lavaud, Fournet, Pironti, and Strub, 2014
- Mitigation: extended\_master\_secret extension ([RFC 7627](https://datatracker.ietf.org/doc/html/rfc7627))
- Bind handshake transcript to key derivation

## Best practice

- Regularly review system configuration
- Tighten the set of cryptographic algorithms as needed

\large

**$\rightarrow$ crypto-policies come to the rescue**

\vspace*{3cm}

## crypto-policies

Provide a system-wide *baseline* of cryptographic algorithms

- Adopted in Fedora, RHEL, openSUSE

Problem: tightning the default is hard

- [StrongCryptoSettings3](https://fedoraproject.org/wiki/Changes/StrongCryptoSettings3) Fedora change -- rejected so far
- Suddenly disabling "status quo" algorithms may break existing use-cases

\large

**$\rightarrow$ Need a way to identify actual usage**

## Best practice

- Regularly review system configuration
- Tighten the set of cryptographic algorithms as needed
  - crypto-policies can help you set the baseline

Is this sufficient?

\pause

\large

**$\rightarrow$ No, implementations also matter**

\normalsize

RHEL includes FIPS 140 certified cryptographic modules: OpenSSL, NSS, GnuTLS, libgcrypt, and the kernel

- Code review, acceptance tests, additional requirements

## Case study: CVE-2023-3089: OpenShift in FIPS mode

OpenShift uses different cryptographic backends depending on whether the system is running in FIPS mode or not

\center

\begin{tikzpicture}[
  box/.style={
    draw,
    rectangle,
    minimum height=0.5cm,
    align=center,
    inner sep=1ex
  }
]
    \node[box, font=\ttfamily, text width=2cm] (stdlib) {\scriptsize Go stdlib};
    \node[box, font=\ttfamily, text width=2cm, fit=(stdlib), label={\scriptsize Non-FIPS crypto}] (non-fips) {};

    \node[box, dotted, right=of non-fips, font=\ttfamily, text width=2cm] (dlopen) {\scriptsize dlopen stub};
    \node[box, font=\ttfamily, text width=2cm, fit=(dlopen), label={\scriptsize FIPS crypto}] (fips) {};

    \node[box, inner sep=3ex, fit=(non-fips) (fips), label={\small OpenShift component}] (App) {};

    \node[box, below=of fips, font=\ttfamily, text width=2cm, label=below:{\small FIPS certified crypto module}] (openssl) {OpenSSL};

	\draw[->] (dlopen) -> (openssl);
\end{tikzpicture}

\flushleft

Switching not working means non-certified implementation is used in FIPS mode

## We need a tool to

**Help tighten algorithm selection on a system**

- Analyze long-term trend of algorithm usage
- Companion tool to crypto-policies

**Help ensure that correct implementation is used**

- Audit cryptographic events at real-time
- Indicate which implementation is used

## crypto-auditing

**Goals**

\color{gray}

Challenges

Architecture

Logging mechanism

Demo

Implementation

Future work

## Goals

- Provide an infrastructure to monitor cryptographic operations happening on a system

- Support all FIPS 140 certified cryptographic modules: OpenSSL, NSS, GnuTLS, libgcrypt, and the kernel

- Support both long-term trend analysis and real-time auditing

## crypto-auditing

**Goals**

**Challenges**

\color{gray}

Architecture

Logging mechanism

Demo

Implementation

Future work

## Challenges 

**Efficiency**

- Shouldn't affect system performance too much

**Privacy**

- Collected information shouldn't reveal user's activity

**Maintainability**

- The collection rules are easily maintainable

## crypto-auditing

**Goals**

**Challenges**

**Architecture**

\color{gray}

Logging mechanism

Demo

Implementation

Future work

## Architecture

![](architecture.pdf){width=150%}

## crypto-auditing

**Goals**

**Challenges**

**Architecture**

**Logging mechanism**

\color{gray}

Demo

Implementation

Future work

## Logging mechanism: Tracing userspace programs with eBPF

**uprobes**

- Monitor enter/exit of function calls
- Pro: No need to modify system components
- Con: Too simple to monitor cryptographic operations

\vspace*{3cm}

## Example: Digital signature creation with OpenSSL API

\small
```c
ctx = EVP_PKEY_CTX_new(signing_key, NULL /* no engine */);
if (!ctx)
	/* Error occurred */
if (EVP_PKEY_sign_init(ctx) <= 0)
	/* Error */
if (EVP_PKEY_CTX_set_rsa_padding(ctx, RSA_PKCS1_PADDING) <= 0)
	/* Error */
if (EVP_PKEY_CTX_set_signature_md(ctx, EVP_sha256()) <= 0)
	/* Error */

if (EVP_PKEY_sign(ctx, sig, &siglen, md, mdlen) <= 0)
	/* Error */
```

## Logging mechanism: Tracing userspace programs with eBPF

**uprobes**

- Monitor enter/exit of function calls
- Pro: No need to modify system components
- Con: Too simple to monitor cryptographic operations

**USDT (user statically defined tracepoint)**

- Arbitrary probe points instrumented in binary
- Pro: Flexibility to define custom probe points
- Con: Need to modify system components

## Logging mechanism: File format

Events are organized in a contextual/hierarchical manner

\begin{sequencediagram}
\tikzstyle{inststyle}=[]
\newthread{t}{}
\newinst[3]{a}{}
\newinst[3]{b}{}
\newinst[1]{c}{}
\begin{call}{t}{\scriptsize tls::handshake\_client}{a}{}
\begin{call}{a}{\scriptsize tls::certificate\_verify}{b}{}
\mess{b}{}{c}
\node[anchor=west] (t0) at (mess to) [text width=1cm]{\scriptsize pk::rsa\_size\\ \tt 3072};
\end{call}
\end{call}
\end{sequencediagram}

This allows same events to be used for different purposes to limit the size of vocabulary

## Logging format: Contexts and events

**Context**

- Defines a period of time where events or other contexts can occur
- Represented as a 16-byte unique identifier, obfuscated by the agent
- Associated with a name

**Event**

- Defines a moment in time
- Represented as a key value pair

## Logging format: Representation

**Context introduction**

- `new_context(context, parent)`

**Event data**

- `word_data(context, key, value)`
- `string_data(context, key, value)`
- `blob_data(context, key, value, value_size)`

## Example: TLS client handshake

\begin{tikzpicture}[
  context/.style={
    draw,
	rectangle,
	align=center,
	font=\ttfamily,
	fill=gray!30
  },
  event/.style={
    draw,
	rectangle,
	align=center,
	font=\ttfamily
  },
  node distance=\pgflinewidth
]

\node[context, text width=11cm] (handshakeclient) {\small tls::handshake\_client\\ \small 00..01};
\node[event, below right=of handshakeclient.south west] (protocolversion) {\small tls::protocol\_version\\ \small 0x0304};
\node[context, below=of handshakeclient.south, right=of protocolversion, text width=6.8cm] (certificateverify) {\small tls::certificate\_verify\\ \small 00..02};
\node[event, below right=of certificateverify.south west] (signaturealgorithm) {\small tls::signature\_algorithm\\ \small 0x0804};
\node[event, below=of certificateverify, right=of signaturealgorithm] (rsasize) {\small pk::rsa\_size\\ \small 3072};

\end{tikzpicture}

## Logging format: Compression and rotation

Primary log file can be indefinitely large over time

**Event grouping**

- Multiple events may often share the same context ID
- Consolidate them into a single log entry based on time window

**Log rotation/sharding**

- The primary log file is automatically split when the size limit has reached

## crypto-auditing

**Goals**

**Challenges**

**Architecture**

**Logging format**

**Demo**

\color{gray}

Implementation

Future work

## Demo

3 scenarios

- Basic operation: collecting and accessing logs
- Visualizing logs as flamegraph
- Spotting legacy key derivation in TLS 1.2

## Demo: Basic operation

1. Start `crypto-auditing-client` on a terminal:
   ```console
   $ crypto-auditing-client --scope tls \
                            --format cbor \
                            --output audit.cborseq
   ```

2. Use TLS on a different terminal:
   ```console
   $ wget https://fedoraproject.org
   ```

3. Parse the log with `crypto-auditing-log-parser`:
   ```console
   $ crypto-auditing-log-parser audit.cborseq
   ```

## Demo: Visualizing logs as flamegraph

1. Render flamegraph with `flamegraph.py` script:
   ```console
   $ crypto-auditing-log-parser audit.cborseq | \
     python scripts/flamegraph.py --format sqlite -
   ```

## Demo: Spotting legacy key derivation in TLS 1.2

1. Access a site supporting EMS
   ```console
   $ wget --ciphers=@SYSTEM:-VERS-TLS1.3 \
          https://fedoraproject.org
   ```

1. Access a site not supporting EMS
   ```console
   $ wget --ciphers=@SYSTEM:-VERS-TLS1.3 \
          https://tls-v1-2.badssl.com:1012/
   ```

3. Check if there are `tls::ext::extended_master_secret` events

## crypto-auditing

**Goals**

**Challenges**

**Architecture**

**Logging format**

**Demo**

**Implementation**

\color{gray}

Future work

## Implementation

Git repository: [github.com/latchset/crypto-auditing](https://github.com/latchset/crypto-auditing)

The components are written in Rust and available on crates.io:

- `crypto-auditing-agent`: Agent service
- `crypto-auditing-event-broker`: Event-broker service
- `crypto-auditing`: Library to talk with event broker
- `crypto-auditing-client`: CLI to talk with event broker
- `crypto-auditing-log-parser`: CLI to parse log files

Using RPM packages is recommended

## Packaging in Fedora Linux

**RPM repository for crypto-auditing components**

- [coprs/ueno/rust-crypto-auditing/](https://copr.fedorainfracloud.org/coprs/ueno/rust-crypto-auditing/)
- Being proposed for inclusion in official Fedora releases

**RPM repository for instrumented libraries**

- [coprs/ueno/crypto-auditing/](https://copr.fedorainfracloud.org/coprs/ueno/crypto-auditing/)
- OpenSSL and GnuTLS are instrumented

## Performance impact: Measurement setup

Perform TLS 1.3 handshake 10000 times[^1]

- Only memory based transport is used (no I/O involved)
- Single-core vs multi-core: emulate a single-core system with the kernel parameter `nr_cpus=1`

\small
```console
$ sudo sysctl -w kernel.perf_event_paranoid=-1
$ perf stat ./benchmark-handshake -c 10000 \
	        -p $(nproc) --priority NORMAL
```

[^1]: https://gitlab.com/dueno/benchmark-handshake

## Performance impact

**Overall time spent (in seconds)**

\small

|             | 1 core     | 8 cores   |
|-------------|------------|-----------|
| with c-a    | 101.491559 | 36.462192 |
| without c-a | 97.027244  | 36.671538 |

\normalsize

**Number of context switches (in times)**

\small

|             | 1 core  | 8 cores |
|-------------|---------|---------|
| with c-a    | 310,079 | 263,965 |
| without c-a | 83,830  | 34,401  |

## Performance impact: Interpretation

- Time-wise, 4% degradation on a single-core system, little to no difference on an 8-core system

- 7 times more context switches are observed on both single-core and multi-core systems

- We expect the difference to be much smaller on practical setup invoving I/O

## crypto-auditing

**Goals**

**Challenges**

**Architecture**

**Logging format**

**Demo**

**Implementation**

**Future work**

## Future work

**Instrumentation**

- Propose instrumentation to the upstream
- Figure out a way of monitoring kernel crypto usage
- Support programming languages with static linking nature

**Misc**

- Grafana data source plug-in for real-time analysis
- Support applications inside a container

## Conclusion

[crypto-auditing](https://github.com/latchset/crypto-auditing) is a tool to identify cryptographic operations happening on a system

- Long-term trend analysis
- Real-time event auditing

The architecture is designed to be fast and simple, while preserving privacy

## Thank you

\vspace*{2cm}
\large
\center
Questions?

\vspace*{3cm}
\tiny
\flushright
Image credit on slide 4: Copyright © irasutoya
